# React chat
App written using socket.io and React


# Useful links:

[https://medium.com/@coderacademy/you-can-build-an-fb-messenger-style-chat-app-with-reactjs-heres-how-intermediate-211b523838ad](https://medium.com/@coderacademy/you-can-build-an-fb-messenger-style-chat-app-with-reactjs-heres-how-intermediate-211b523838ad)

[simple-chat-api](https://github.com/kentandlime/simple-chat-api)

[socket.io](https://socket.io/get-started/chat/)