require('../styles/App.css');
require('../styles/Login.css');

import React from 'react';
import ChatApp from './ChatApp';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { username: '' };

    this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
    this.usernameSubmitHandler = this.usernameSubmitHandler.bind(this);
  }

  usernameChangeHandler(event) {
    this.setState({ username: event.target.value });
  }

  usernameSubmitHandler(event) {
    event.preventDefault();
    this.setState({ submitted: true, username: this.state.username });
  }

  render() {
    if (this.state.submitted) {
      //if user is logged
      return (
        <ChatApp username={this.state.username} />
      );
    }

    //first page load
    return (
      <form onSubmit={this.usernameSubmitHandler} className="username-container">
        <h1>Czat</h1>
        <div>
          <input
            type="text"
            onChange={this.usernameChangeHandler}
            placeholder="Podaj nazwe użytkownika"
            required />
        </div>
        <input type="submit" value="Zaloguj" />
      </form>
    );
  }

}
App.defaultProps = {
};
export default App;
