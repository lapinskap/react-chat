require('../styles/ChatApp.css');

import React from 'react';
import io from 'socket.io-client';
import config from '../config';

import Messages from './Messages';
import ChatInput from './ChatInput';

class ChatApp extends React.Component {
  socket = {};
  constructor(props) {
    super(props);
    this.state = { messages: []
    };
    this.sendHandler = this.sendHandler.bind(this);
    
    //Server connection
    this.socket = io(config.api, { query: `username=${props.username}` }).connect();

    this.socket.on('server:message', message => {
      this.addMessage(message);
    });
  }

  sendHandler(message) {
    const messageObject = {
      username: this.props.username,
      message
    };

    //Emiting message to the server
    this.socket.emit('client:message', messageObject);

    messageObject.fromMe = true;
    this.addMessage(messageObject);
  }

  addMessage(message) {
    const messages = this.state.messages;
    messages.push(message);
    this.setState({ messages });
  }

  render() {
    return (
      <div className="container">
      <table>
        <th>
        <div className="Czat">
          <h3>Cześć, {this.props.username}!</h3>
          <Messages messages={this.state.messages} />
          <ChatInput onSend={this.sendHandler} />
        </div>
        </th>

        <th>
        <div className="users">
          <h3>Logged Users:</h3>
          <p>{this.props.username}</p>
        </div>
        </th>
      </table>
      </div>
    );
  }

}
ChatApp.defaultProps = {
  username: 'Anonymous'
};

export default ChatApp;
